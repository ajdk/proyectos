from django import forms
from .models import Usuario
from .models import Documento

class CreacionUsuario(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [
            'codigoUsuario',
            'nombreUsuario',
            'relacion',
            'descripcion'
        ]

class CreacionDocumento(forms.ModelForm):
    class Meta:
        model = Documento
        fields = [
            'nombreDocumento',
            'documento',
        ]
