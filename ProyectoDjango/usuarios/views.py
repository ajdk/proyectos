from django.shortcuts import render, get_object_or_404, redirect
from .models import Usuario, Documento
from .forms import CreacionUsuario, CreacionDocumento

def usuario_delete_view(request, codigoUsuario):

    obj = get_object_or_404(Usuario, codigoUsuario=codigoUsuario)
    if request.method == "POST":
        obj.delete()
        return redirect('/administrador')
    context = {
        "object" : obj
    }
    return render(request, "Usuarios/borrarusuario.html", context)


def documento_delete_view(request, nombreDocumento):

    obj = get_object_or_404(Documento, nombreDocumento=nombreDocumento)
    if request.method == "POST":
        obj.documento.delete()
        obj.delete()
        return redirect('/administrador')
    context = {
        "object" : obj
    }
    return render(request, "Usuarios/borrardocumento.html", context)



def documento_create_view(request):
    form = CreacionDocumento(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        return redirect('/administrador')
    context = {
        'form' : form

    }


    return render(request, "Usuarios/creaciondocumento.html", context)


def usuario_create_view(request):
    form = CreacionUsuario(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/administrador')
    context = {
        'form' : form

    }

    return render(request, "Usuarios/creacionusuario.html", context)

def usuario_detail_view(request, codigoUsuario):

    obj = get_object_or_404(Usuario, codigoUsuario=codigoUsuario)
    context = {
        'objetct' : obj

    }

    return render(request, "Usuarios/detail.html", context)
