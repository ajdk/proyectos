from django.db import models
from django.urls import reverse
from django.shortcuts import redirect

class Usuario(models.Model):
    codigoUsuario = models.CharField(max_length=50, null=False, primary_key=True)
    nombreUsuario = models.TextField()
    relacion = models.ManyToManyField('Documento', blank=True)
    descripcion = models.TextField()

    def get_absolute_url(self):
        return reverse("usuario", kwargs={'codigoUsuario': self.codigoUsuario})

class Documento(models.Model):
    nombreDocumento = models.CharField(max_length=50, primary_key=True)
    documento= models.FileField()




