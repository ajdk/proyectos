from django.shortcuts import render
from usuarios.models import Usuario, Documento
# Create your views here.

def administrador_view(request):

    users = Usuario.objects.all()
    documents = Documento.objects.all()

    context = {
        'usuarios': users,
        'documentos': documents
    }

    return render(request, "administrador.html", context)
