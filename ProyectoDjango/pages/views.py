from django.shortcuts import render
from django.shortcuts import redirect

def home_view(request, *args, **kwargs):
    query = request.GET.get("q")

    if query:

        return redirect('usuario', query)
    return render(request, "home.html", {})

