#!/usr/bin/env python

from hardware import *
import log


## emulates a compiled program
class Program():

    def __init__(self,instructions):
        self._instructions = self.expand(instructions)


    @property
    def instructions(self):
        return self._instructions

    def addInstr(self, instruction):
        self._instructions.append(instruction)

    def expand(self, instructions):
        expanded = []
        for i in instructions:
            if isinstance(i, list):
                ## is a list of instructions
                expanded.extend(i)
            else:
                ## a single instr (a String)
                expanded.append(i)

        ## now test if last instruction is EXIT
        ## if not... add an EXIT as final instruction
        last = expanded[-1]
        if not ASM.isEXIT(last):
            expanded.append(INSTRUCTION_EXIT)

        return expanded

    def __repr__(self):
        return "Program({instructions})".format( instructions=self._instructions)


## emulates an Input/Output device controller (driver)
class IoDeviceController():

    def __init__(self, device):
        self._device = device
        self._waiting_queue = []
        self._currentPCB = None

    def runOperation(self, pcb, instruction):
        pair = {'pcb': pcb, 'instruction': instruction}
        # append: adds the element at the end of the queue
        self._waiting_queue.append(pair)
        # try to send the instruction to hardware's device (if is idle)
        self.__load_from_waiting_queue_if_apply()

    def getFinishedPCB(self):
        finishedPCB = self._currentPCB
        self._currentPCB = None
        self.__load_from_waiting_queue_if_apply()
        return finishedPCB

    def __load_from_waiting_queue_if_apply(self):
        if (len(self._waiting_queue) > 0) and self._device.is_idle:
            ## pop(): extracts (deletes and return) the first element in queue
            pair = self._waiting_queue.pop(0)
            # print(pair)
            pcb = pair['pcb']
            instruction = pair['instruction']
            self._currentPCB = pcb
            self._device.execute(instruction)

    def __repr__(self):
        return "IoDeviceController for {deviceID} running: {currentPCB} waiting: {waiting_queue}".format(
            deviceID=self._device.deviceId, currentPCB=self._currentPCB, waiting_queue=self._waiting_queue)


## emulates the  Interruptions Handlers
class AbstractInterruptionHandler():
    def __init__(self, kernel):
        self._kernel = kernel

    @property
    def kernel(self):
        return self._kernel

    def execute(self, irq):
        log.logger.error("-- EXECUTE MUST BE OVERRIDEN in class {classname}".format(classname=self.__class__.__name__))

    def unloadRunningProgram(self, newState):
        pcb = self.kernel.pcbtable.running
        self.kernel.pcbtable.running = None
        self.kernel.dispatcher.save(pcb)
        pcb.state = newState
        return pcb

    def loadNextProgram(self):
        nextPCB = self.kernel.scheduler.nextProgram()

        log.logger.info("next pcb: {pcb}".format(pcb=nextPCB))

        if (not nextPCB is None):
            self.kernel.pcbtable.running = nextPCB
            self.kernel.dispatcher.load(nextPCB)
            nextPCB.state = States.RUNNING

        return nextPCB

class KillInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        log.logger.info("killing Program")
        self.unloadRunningProgram(States.TERMINATED)
        self.loadNextProgram()
        self.kernel.pcbtable.freeRunningFrames()

class IoInInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        pcb = self.unloadRunningProgram(States.WAITING)
        operation = irq.parameters
        self.kernel.ioDeviceController.runOperation(pcb, operation)
        log.logger.info(self.kernel.ioDeviceController)

        self.loadNextProgram()

class IoOutInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        pcb = self.kernel.ioDeviceController.getFinishedPCB()
        if (self.kernel.pcbtable.running == None):
            self.kernel.pcbtable.running = pcb
            pcb.state = States.RUNNING
            self.kernel.dispatcher.load(pcb)
        else:
            pcb.state = States.READY
            self.kernel.scheduler.addProgram(pcb)

        log.logger.info(self.kernel.ioDeviceController)

class NewInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        program = irq.parameters
        priority = irq.priority
        pagesInFrames = self.kernel.loader.loadProgram(program)
        newPCB = self.kernel.pcbtable.createPCB(pagesInFrames, priority)  # ver estado de new.
        runningPCB = self.kernel.pcbtable.getRunning()
        log.logger.info("new pcb {pid} {pc} {dirbase}".format(pid=newPCB.pid, pc=newPCB.pc, dirbase=newPCB.dirBase))

        if self.kernel.pcbtable.running is None:
            newPCB.state = States.RUNNING
            self.kernel.pcbtable.running = newPCB
            self.kernel.dispatcher.load(newPCB)
            log.logger.info("Program Running")
        elif self.kernel.scheduler.mustExpropiate(runningPCB, newPCB):

            pcb = self.unloadRunningProgram(States.READY)
            self.kernel.scheduler.addProgram(pcb)

            newPCB.state = States.RUNNING
            self.kernel.pcbtable.running = newPCB
            self.kernel.dispatcher.load(newPCB)

            log.logger.info("Program Must be Expropriated")
        else:
            newPCB.state = States.READY
            self.kernel.scheduler.addProgram(newPCB)
            log.logger.info(" Program in Ready Queue")

class TimeOutIRQInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        self.kernel.scheduler.addProgram(self.unloadRunningProgram(States.READY))
        self.loadNextProgram()
        log.logger.info("Completed Quantum Ticks, Next Program")

class States():
    RUNNING = "Running"
    WAITING = "Waiting"
    READY = "Ready"
    NEW = "New"
    TERMINATED = "Terminated"


# emulates the core of an Operative System

class Kernel():

    def __init__(self, Sched, Quant = 0, frame = 0):
        ## setup interruption handlers
        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        newHandler = NewInterruptionHandler(self)
        HARDWARE.interruptVector.register(NEW_INTERRUPTION_TYPE, newHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        timeOutIRQHandler = TimeOutIRQInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timeOutIRQHandler)

        ## controls the Hardware's I/O Device
        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)
        self.dispatcher = Dispatcher()
        self._scheduler = Sched()
        self.dispatcher.setQuantum(Quant)
        if frame is not None:
            HARDWARE.mmu.frameSize = frame
        self.memoryManager = MemoryManager(frame)
        self.loader = Loader(frame, self.memoryManager, self.dispatcher)
        self.pcbtable = PCBTable(self.memoryManager)
        self.fileSystem = FileSystem()
    @property
    def scheduler(self):
        return self._scheduler

    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    def run(self, path, priority = 0):
        ins = self.fileSystem.read(path)
        newIRQ = IRQ(NEW_INTERRUPTION_TYPE, ins, priority)
        HARDWARE.interruptVector.handle(newIRQ)

    def __repr__(self):
        return "Kernel "

class Loader():
    def __init__(self, frameSi, memoryMan, disp):
        self.nextMemorySlot = 0
        self.framseSize = frameSi
        self.memoryManager = memoryMan
        self.dispatcher = disp

    def loadProgram(self, prog):

        if len(prog.instructions) == 0:
            return

        programInstructions = prog.instructions
        numberOfPages = len(prog.instructions) // self.framseSize
        if (len(prog.instructions) % self.framseSize > 0):
            numberOfPages = numberOfPages + 1
        framesToWrite = self.memoryManager.giveMeFrames(numberOfPages)
        pagesInFrames = []

        while len(framesToWrite) > 0:
            instructionsToWrite = self.divideByFrame(programInstructions)
            for i in instructionsToWrite:
                writingFrame = framesToWrite.pop(0)
                self.loadFrames(writingFrame, i)
                pagesInFrames.append(writingFrame)
        log.logger.info("Loading Frames: {pageinFrames}".format(pageinFrames= pagesInFrames))
        log.logger.info(HARDWARE.memory)

        return pagesInFrames


    def divideByFrame(self, listOfInstructions):

        pages = []
        page = []

        for index,instruction in  enumerate(listOfInstructions):
            if len(page) < self.framseSize:
                page.append(instruction)
            else:
                pages.append(page)
                page=[]
                page.append(instruction)
        if len(page)>0:
            pages.append(page)
        return pages


    def loadFrames(self, frameToWrite, instructionsToWrite):

        memoryDirection = frameToWrite * self.framseSize

        for index in range(0, len(instructionsToWrite) ):

            HARDWARE.memory.write(memoryDirection, instructionsToWrite[index])
            memoryDirection = memoryDirection + 1

        log.logger.info(HARDWARE.memory)

class PCB():

    def __init__(self, pid, pagesInFrames, priority):
        self._state = States.NEW
        self._pId = pid
        self._pc = 0
        self._pagesInFrames = pagesInFrames
        self._priority = priority

    @property
    def priority(self):
        return self._priority

    @priority.setter
    def priority(self, value):
        self._priority = value

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value

    @property
    def pid(self):
        return self._pId

    @property
    def pc(self):
        return self._pc

    @pc.setter
    def pc(self, value):
        self._pc = value

    @property
    def dirBase(self):
        return self._pagesInFrames [0]

    def frameInPages(self):
        return self._pagesInFrames

    def returnPage(self, page):
        return self._pagesInFrames[page]


class PCBTable():
    def __init__(self, memoryMan):
        self._running = None
        self._programs = []
        self._pIdCounter = 0
        self._memoryManager = memoryMan

    def newPId(self):
        self._pIdCounter = self._pIdCounter +1
        return self._pIdCounter

    def createPCB(self, pagesInFrames, priority):
        pid = self.newPId()
        newPCB = PCB(pid, pagesInFrames, priority)
        self._programs.append([newPCB, pagesInFrames])
        return newPCB

    def getRunning(self):
        return self._running

    @property
    def running(self):
        return self._running

    @running.setter
    def running(self, pcb):
        self._running = pcb

    def freeRunningFrames(self):
        if self._running == None:
            return
        newFreeFrames = self._running.frameInPages()
        self._memoryManager.newFreeFrames(newFreeFrames)


class Dispatcher():

    def load(self, pcb):
        HARDWARE.timer.reset()
        HARDWARE.mmu.resetTLB()
        for i in range(0, len(pcb.frameInPages())):
            HARDWARE.mmu.setPageFrame(i, pcb.returnPage(i))
            log.logger.info("loading Page {page}->{frame}".format(page=i, frame=pcb.returnPage(i)))
        # HARDWARE.mmu.baseDir = pcb.dirBase

        HARDWARE.cpu.pc = pcb.pc

    def save(self, pcb):
        pcb.pc = HARDWARE.cpu.pc
        HARDWARE.cpu.pc = -1

    def setQuantum(self, quant):
        if quant != 0:
            HARDWARE.timer.quantum = quant


class PcbWithPriority():

    def __init__(self, pcb):
        self.thisPcb = pcb
        self.pcbPriority = pcb.priority

    def pcb(self):
        return self.thisPcb

    def priority(self):
        return self.pcbPriority

    def increasePriority(self):
        if self.priority() < 10:
            self.pcbPriority += self.pcbPriority

    def dirBase(self):
        return self.thisPcb.dirBase

    def pc(self):
        return self.thisPcb.pc


class SchedulerFCFS():
    def __init__(self):
        self._readyQueue = []

    def addProgram(self, pcb):
        self._readyQueue.append(pcb)

    def nextProgram(self):
        if self.isEmpty():
            return None
        return self._readyQueue.pop(0)

    def isEmpty(self):
        return len(self._readyQueue) == 0

    def mustExpropiate(self, newPcb, pcbRunning):
        return False


class SchedulerPriority():

    def __init__(self):
        self._readyQueue = []

    def mustExpropiate(self, newPcb, pcbRunning):
        return False

    def addProgram(self, pcb):
        _newPcbPriority = PcbWithPriority(pcb)
        self._readyQueue.append(_newPcbPriority)

    def increaseAllPriority(self):
        for i in self._readyQueue:
            i.increasePriority()

    def nextProgram(self):
        if (self.isEmpty()):
            return None
        self.increaseAllPriority()
        return self._readyQueue.pop(self.highestPriorityPcb()).pcb()

    def isEmpty(self):
        return len(self._readyQueue) == 0

    def highestPriorityPcb(self):

        _highestPriority = 0
        _counter = 0
        _counterBestPcb = 0

        for i in self._readyQueue:

            if i.priority() > _highestPriority:
                _highestPriority = i.priority()
                _counterBestPcb = _counter

            _counter = _counter + 1

        return _counterBestPcb


class SchedulerPriorityPreemptive():

    def __init__(self):
        self._readyQueue = []

    def mustExpropiate(self, pcbRunning, newPcb):
        return newPcb.priority > pcbRunning.priority

    def addProgram(self, pcb):
        _newPcbPriority = PcbWithPriority(pcb)
        self._readyQueue.append(_newPcbPriority)

    def increaseAllPriority(self):
        for i in self._readyQueue:
            i.increasePriority()

    def nextProgram(self):
        if (self.isEmpty()):
            return None
        self.increaseAllPriority()
        return self._readyQueue.pop(self.highestPriorityPcb()).pcb()

    def isEmpty(self):
        return len(self._readyQueue) == 0

    def highestPriorityPcb(self):

        _highestPriority = 0
        _counter = 0
        _counterBestPcb = 0

        for i in self._readyQueue:

            if i.priority() > _highestPriority:
                _highestPriority = i.priority()
                _counterBestPcb = _counter

            _counter = _counter + 1

        return _counterBestPcb

class MemoryManager:

    def __init__(self, frameSize):

        self._frameSize = frameSize
        self._freeFrames = []
        self._busyFrames = []

        numberOfFrames = HARDWARE.memory.size // frameSize
        for i in range(0, numberOfFrames):
            self._freeFrames.append(i)

    def giveMeFrames(self, number):
        pages= []

        for i in range(0, number):
            pages.append(self._freeFrames.pop(0))

        return pages

    def newFreeFrames(self, newFreeFrames):
        self._freeFrames.append(newFreeFrames)
        
        
class FileSystem():

        def __init__(self):
            self._fileSystem = {}

        def write(self, name, program):
            self._fileSystem[name] = program


        # Precondicion: El nombre debe existir
        def read(self, name):
            programToReturn = self._fileSystem[name]
            del self._fileSystem[name]

            return programToReturn
