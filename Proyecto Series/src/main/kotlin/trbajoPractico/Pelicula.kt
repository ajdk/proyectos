package trbajoPractico

class Pelicula(){

    var estado : Estado = Deshabilitada()
    var descripcion : String = ""
    var poster : String = ""
    var titulo : String = ""
    var video : String = ""
    var duracion : Int = 0
    var actores : List<String> = emptyList()
    var directores : List<String> = emptyList()
    var categorias : List<String> = emptyList()
    var contenidoRelacionado : String = ""


}